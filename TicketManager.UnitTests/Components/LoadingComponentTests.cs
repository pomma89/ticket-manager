using Bunit;

using TicketManager.Shared;

namespace TicketManager.UnitTests.Components;

public sealed class LoadingComponentTests
{
    [Test]
    public void ComponentShouldRenderLoadingLabel()
    {
        // Arrange
        using var ctx = new Bunit.TestContext();

        // Act
        var cut = ctx.RenderComponent<Loading>();

        // Assert
        Assert.That(cut.Markup, Contains.Substring("Loading"));
    }
}