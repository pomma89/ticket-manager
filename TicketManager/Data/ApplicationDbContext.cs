﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace TicketManager.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<TagGroup> TagGroups => Set<TagGroup>();
        public DbSet<Tag> Tags => Set<Tag>();
        public DbSet<Ticket> Tickets => Set<Ticket>();

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<IdentityRole>().HasData(
                new IdentityRole(Constants.Roles.Admin) { Id = "80a51212-de89-4a09-85bc-cfb6b6ee84d1" }
            );

            builder.Entity<TagGroup>().HasData(
                new TagGroup(Models.Enums.TagGroup.Category, "Category"),
                new TagGroup(Models.Enums.TagGroup.OperatingSystem, "Operating System"),
                new TagGroup(Models.Enums.TagGroup.Priority, "Priority"),
                new TagGroup(Models.Enums.TagGroup.Product, "Product")
            );
        }
    }
}