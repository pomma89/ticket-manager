﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TicketManager.Data.Migrations
{
    public partial class SeedTagGroups2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80a51212-de89-4a09-85bc-cfb6b6ee84d1",
                column: "ConcurrencyStamp",
                value: "b074545c-c52c-4c1f-b9ec-e526239896d9");

            migrationBuilder.InsertData(
                table: "TagGroups",
                columns: new[] { "Id", "Name" },
                values: new object[] { (byte)4, "Product" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "TagGroups",
                keyColumn: "Id",
                keyValue: (byte)4);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80a51212-de89-4a09-85bc-cfb6b6ee84d1",
                column: "ConcurrencyStamp",
                value: "28305ba7-f4de-4492-ad82-123013affc0a");
        }
    }
}
