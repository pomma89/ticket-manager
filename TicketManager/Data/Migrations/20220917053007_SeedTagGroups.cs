﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TicketManager.Data.Migrations
{
    public partial class SeedTagGroups : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "TagGroups",
                columns: new[] { "Id", "Name" },
                values: new object[] { (byte)2, "Operating System" });

            migrationBuilder.InsertData(
                table: "TagGroups",
                columns: new[] { "Id", "Name" },
                values: new object[] { (byte)3, "Priority" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "TagGroups",
                keyColumn: "Id",
                keyValue: (byte)2);

            migrationBuilder.DeleteData(
                table: "TagGroups",
                keyColumn: "Id",
                keyValue: (byte)3);
        }
    }
}
