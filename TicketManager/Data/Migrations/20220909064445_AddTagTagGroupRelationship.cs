﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TicketManager.Data.Migrations
{
    public partial class AddTagTagGroupRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte>(
                name: "GroupId",
                table: "Tags",
                type: "tinyint",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.CreateIndex(
                name: "IX_Tags_GroupId",
                table: "Tags",
                column: "GroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tags_TagGroups_GroupId",
                table: "Tags",
                column: "GroupId",
                principalTable: "TagGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tags_TagGroups_GroupId",
                table: "Tags");

            migrationBuilder.DropIndex(
                name: "IX_Tags_GroupId",
                table: "Tags");

            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "Tags");
        }
    }
}
