﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TicketManager.Data.Migrations
{
    public partial class AddExternalCodeToTicket : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1fb8cdc1-bdc0-4223-9675-0aeb498524ff");

            migrationBuilder.AddColumn<string>(
                name: "ExternalCode",
                table: "Tickets",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: false,
                defaultValue: "");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "80a51212-de89-4a09-85bc-cfb6b6ee84d1", "28305ba7-f4de-4492-ad82-123013affc0a", "Admin", null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80a51212-de89-4a09-85bc-cfb6b6ee84d1");

            migrationBuilder.DropColumn(
                name: "ExternalCode",
                table: "Tickets");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "1fb8cdc1-bdc0-4223-9675-0aeb498524ff", "4eeae050-f896-4678-8a45-282b0e5a48f9", "Admin", null });
        }
    }
}
