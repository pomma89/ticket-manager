﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TicketManager.Data.Migrations
{
    public partial class AddAttachmentToTicket : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "Attachment",
                table: "Tickets",
                type: "varbinary(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80a51212-de89-4a09-85bc-cfb6b6ee84d1",
                column: "ConcurrencyStamp",
                value: "204a8110-d4f6-49ec-abeb-edaebaffc3fc");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Attachment",
                table: "Tickets");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "80a51212-de89-4a09-85bc-cfb6b6ee84d1",
                column: "ConcurrencyStamp",
                value: "b074545c-c52c-4c1f-b9ec-e526239896d9");
        }
    }
}
