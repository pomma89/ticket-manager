﻿using System.ComponentModel.DataAnnotations;
using TagGroupEnum = TicketManager.Models.Enums.TagGroup;

namespace TicketManager.Data;

public class Tag
{
    public int Id { get; set; }

    public TagGroup Group { get; set; } = default!;

    public TagGroupEnum GroupId { get; set; }

    [Required, StringLength(Validations.MaxNameLength)]
    public string Name { get; set; } = default!;

    public ICollection<Ticket> Tickets { get; set; } = default!;

    public static class Validations
    {
        public const int MaxNameLength = 50;
    }
}
