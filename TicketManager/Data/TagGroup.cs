﻿using System.ComponentModel.DataAnnotations;
using TagGroupEnum = TicketManager.Models.Enums.TagGroup;

namespace TicketManager.Data;

public class TagGroup
{
    public TagGroup(TagGroupEnum id, string name)
    {
        Id = id;
        Name = name;
    }

    public TagGroupEnum Id { get; set; }

    [Required, StringLength(50)]
    public string Name { get; set; }
}
