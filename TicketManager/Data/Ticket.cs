﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace TicketManager.Data;

public class Ticket
{
    public byte[]? Attachment { get; set; }

    public DateTimeOffset CreatedAt { get; set; }

    public IdentityUser CreatedBy { get; set; } = default!;

    [Required, StringLength(Validations.MaxDescriptionLength)]
    public string Description { get; set; } = default!;

    [Required, StringLength(Validations.MaxExternalCodeLength)]
    public string ExternalCode { get; set; } = default!;

    public int Id { get; set; }

    public ICollection<Tag> Tags { get; set; } = default!;

    [Required, StringLength(Validations.MaxTitleLength, MinimumLength = Validations.MinTitleLength)]
    public string Title { get; set; } = default!;

    public static class Validations
    {
        public const int MaxDescriptionLength = 2000;
        public const int MaxExternalCodeLength = 20;
        public const int MinTitleLength = 1;
        public const int MaxTitleLength = 200;
    }
}
