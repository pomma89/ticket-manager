﻿using System.Text.Json;
using System.Text.Json.Nodes;

using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;

namespace TicketManager.Shared;

public sealed class Templater : ComponentBase
{
    private const string ComponentTypeNode = "$type";

    private static readonly string RootNamespace = typeof(Templater).Namespace!;
    private static readonly JsonDocumentOptions JsonDocumentOptions = new JsonDocumentOptions { AllowTrailingCommas = true };

    protected override void BuildRenderTree(RenderTreeBuilder builder)
    {
        base.BuildRenderTree(builder);

        JsonArray parsedComponents = ParseComponentsJson();
        var counter = new Counter();

        foreach (var parsedComponent in parsedComponents)
        {
            var componentType = GetComponentType(parsedComponent);
            BuildComponent(builder, counter, componentType, parsedComponent);
        }
    }

    private static void BuildComponent(RenderTreeBuilder builder, Counter counter, Type componentType, JsonNode parsedComponent)
    {
        builder.OpenRegion(counter.Count++);
        builder.OpenComponent(0, componentType);

        var componentBuilder = componentType.GetMethods().Single(m => m.Name == "BuildFromJson");
        componentBuilder.Invoke(null, new object[] { builder, parsedComponent.AsObject(), counter });

        builder.CloseComponent();
        builder.CloseRegion();
    }

    private static Type GetComponentType(JsonNode parsedComponent)
    {
        var componentTypeName = parsedComponent[ComponentTypeNode];
        return Type.GetType($"{RootNamespace}.{componentTypeName}")!;
    }

    private static JsonArray ParseComponentsJson()
    {
        return JsonNode.Parse(@"[
            {
                ""$type"": ""LinkButton"",
                ""Text"": ""Prova 1"",
            },
            {
                ""$type"": ""Loading"",
            },
            {
                ""$type"": ""LinkButton"",
                ""Text"": ""Prova 2"",
                ""Type"": ""Secondary"",
            },
        ]", documentOptions: JsonDocumentOptions)!.AsArray();
    }

    public sealed class Counter
    {
        public int Count { get; set; }
    }
}
