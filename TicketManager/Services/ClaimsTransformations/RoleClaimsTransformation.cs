﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace TicketManager.Services.ClaimsTransformations;

public sealed class RoleClaimsTransformation : IClaimsTransformation
{
    private readonly UserManager<IdentityUser> _userManager;

    public RoleClaimsTransformation(UserManager<IdentityUser> userManager)
    {
        _userManager = userManager;
    }

    public async Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
    {
        var userIdentity = principal.Identity as ClaimsIdentity;
        var user = await _userManager.FindByNameAsync(userIdentity!.Name);
        var userRoles = await _userManager.GetRolesAsync(user);
        foreach (var role in userRoles)
        {
            var roleClaim = new Claim(userIdentity.RoleClaimType, role);
            userIdentity.AddClaim(roleClaim);
        }
        return principal;
    }
}
