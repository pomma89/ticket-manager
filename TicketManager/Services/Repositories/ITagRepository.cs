﻿using TicketManager.Models.DTO;

namespace TicketManager.Services.Repositories;

public interface ITagRepository
{
    Task AddTagAsync(NewTag newTag);

    Task<IEnumerable<TagDetails>> GetAllTagsAsync();

    Task<IEnumerable<TagGroupDetails>> GetAllTagGroupsAsync();

    Task RemoveTagAsync(int tagId);
}
