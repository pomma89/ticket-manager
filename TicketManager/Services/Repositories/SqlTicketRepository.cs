﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using TicketManager.Data;
using TicketManager.Models.DTO;

namespace TicketManager.Services.Repositories;

public sealed class SqlTicketRepository : ITicketRepository
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly ISystemClock _clock;
    private readonly UserManager<IdentityUser> _userManager;

    public SqlTicketRepository(
        ApplicationDbContext dbContext, 
        IHttpContextAccessor httpContextAccessor,
        ISystemClock clock,
        UserManager<IdentityUser> userManager)
    {
        _dbContext = dbContext;
        _httpContextAccessor = httpContextAccessor;
        _clock = clock;
        _userManager = userManager;
    }

    public async Task AddTicketAsync(NewTicket newTicket)
    {
        var userName = _httpContextAccessor.HttpContext.User.Identity.Name;
        var user = await _userManager.FindByNameAsync(userName);

        var tags = await _dbContext.Tags
            .Where(x => newTicket.TagIds.Contains(x.Id))
            .ToListAsync();

        _dbContext.Tickets.Add(new Ticket
        {
            Title = newTicket.Title,
            ExternalCode = newTicket.ExternalCode,
            Description = newTicket.Description,
            CreatedAt = _clock.UtcNow,
            CreatedBy = user,
            Tags = tags,
            Attachment = newTicket.Attachment,
        });
        
        await _dbContext.SaveChangesAsync();
    }

    public async Task<IEnumerable<TicketSummary>> GetAllTicketSummariesAsync()
    {
        var userName = _httpContextAccessor.HttpContext.User.Identity.Name;
        var user = await _userManager.FindByNameAsync(userName);
        var userIsAdmin = _httpContextAccessor.HttpContext.User.HasClaim(x => x.Type == ClaimTypes.Role && x.Value == Constants.Roles.Admin);

        return await _dbContext.Tickets
            .Where(x => userIsAdmin || x.CreatedBy.Id == user.Id)
            .Select(x => new TicketSummary(x.Id, x.Title, x.ExternalCode, x.CreatedBy.NormalizedUserName, x.CreatedAt))
            .ToListAsync();
    }

    public async Task<TicketDetails?> GetTicketDetailsAsync(int ticketId)
    {
        return await _dbContext.Tickets
            .Where(x => x.Id == ticketId)
            .Select(x => new TicketDetails(
                x.Id, x.Title, x.ExternalCode, x.CreatedBy.NormalizedUserName, x.CreatedAt, x.Description, 
                x.Tags.OrderBy(t => t.Group.Name).ThenBy(t => t.Name).Select(t => new TagDetails(t.Id, t.Name, t.GroupId, t.Group.Name)),
                x.Attachment != null))
            .SingleOrDefaultAsync();
    }

    public async Task<byte[]?> GetTicketAttachmentAsync(int ticketId)
    {
        return await _dbContext.Tickets
            .Where(x => x.Id == ticketId)
            .Select(x => x.Attachment)
            .SingleOrDefaultAsync();
    }

    public async Task RemoveTicketAsync(int ticketId)
    {
        var ticket = await _dbContext.Tickets.SingleAsync(x => x.Id == ticketId);
        _dbContext.Tickets.Remove(ticket);
        await _dbContext.SaveChangesAsync();
    }
}
