﻿using Microsoft.EntityFrameworkCore;
using TicketManager.Data;
using TicketManager.Models.DTO;

namespace TicketManager.Services.Repositories;

public sealed class SqlTagRepository : ITagRepository
{
    private readonly ApplicationDbContext _dbContext;

    public SqlTagRepository(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task AddTagAsync(NewTag newTag)
    {
        _dbContext.Tags.Add(new Tag
        {
            GroupId = newTag.GroupId,
            Name = newTag.Name,
        });
        await _dbContext.SaveChangesAsync();
    }

    public async Task<IEnumerable<TagGroupDetails>> GetAllTagGroupsAsync()
    {
        return await _dbContext.TagGroups
            .Select(x => new TagGroupDetails(x.Id, x.Name))
            .ToListAsync();
    }

    public async Task<IEnumerable<TagDetails>> GetAllTagsAsync()
    {
        return await _dbContext.Tags
            .Select(x => new TagDetails(x.Id, x.Name, x.GroupId, x.Group.Name))
            .ToListAsync();
    }

    public async Task RemoveTagAsync(int tagId)
    {
        var tag = await _dbContext.Tags.SingleAsync(x => x.Id == tagId);
        _dbContext.Tags.Remove(tag);
        await _dbContext.SaveChangesAsync();
    }
}
