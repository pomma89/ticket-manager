﻿using TicketManager.Models.DTO;

namespace TicketManager.Services.Repositories;

public interface ITicketRepository
{
    Task AddTicketAsync(NewTicket newTicket);

    Task<IEnumerable<TicketSummary>> GetAllTicketSummariesAsync();

    Task<TicketDetails?> GetTicketDetailsAsync(int ticketId);

    Task<byte[]?> GetTicketAttachmentAsync(int ticketId);

    Task RemoveTicketAsync(int ticketId);
}
