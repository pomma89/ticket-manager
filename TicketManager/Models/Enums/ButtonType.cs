﻿namespace TicketManager.Models.Enums;

public enum ButtonType
{
    Primary,
    Secondary,
}
