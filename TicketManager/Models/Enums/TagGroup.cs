﻿namespace TicketManager.Models.Enums;

public enum TagGroup : byte
{
    None = 0,
    Category = 1,
    OperatingSystem = 2,
    Priority = 3,
    Product = 4,
}
