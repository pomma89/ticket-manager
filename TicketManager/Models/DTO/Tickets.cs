﻿using System.ComponentModel.DataAnnotations;
using TicketManager.Data;

namespace TicketManager.Models.DTO;

public record TicketSummary(int Id, string Title, string ExternalCode, string CreatedByUserName, DateTimeOffset CreatedAt);

public record TicketDetails(int Id, string Title, string ExternalCode, string CreatedByUserName, DateTimeOffset CreatedAt, string Description, IEnumerable<TagDetails> Tags, bool HasAttachment) 
    : TicketSummary(Id, Title, ExternalCode, CreatedByUserName, CreatedAt);

public sealed class NewTicket
{
    public byte[]? Attachment { get; set; }

    [Required, StringLength(Ticket.Validations.MaxDescriptionLength)]
    public string Description { get; set; } = string.Empty;

    [StringLength(Ticket.Validations.MaxExternalCodeLength)]
    public string ExternalCode { get; set; } = string.Empty;

    [Required, StringLength(Ticket.Validations.MaxTitleLength, MinimumLength = Ticket.Validations.MinTitleLength)]
    public string Title { get; set; } = string.Empty;

    public IList<int> TagIds { get; set; } = Array.Empty<int>();
}