﻿using System.ComponentModel.DataAnnotations;
using TicketManager.Data;
using TagGroupEnum = TicketManager.Models.Enums.TagGroup;

namespace TicketManager.Models.DTO;

public record TagGroupDetails(TagGroupEnum Id, string Name);

public record TagDetails(int Id, string Name, TagGroupEnum GroupId, string GroupName);

public sealed class NewTag
{
    public TagGroupEnum GroupId { get; set; }

    [Required, StringLength(Tag.Validations.MaxNameLength)]
    public string Name { get; set; } = string.Empty;
}
